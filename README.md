## Teste Front-End React JS

Olá caro desenvolvedor front-end, nesse teste analisaremos seu conhecimento geral e inclusive velocidade de desenvolvimento. Abaixo explicaremos tudo o que será necessário, lembrando que, aumento de complexidade, foco em performance, serão pontos positivos na sua avaliação.

### Descrição do teste

Objetivo deste teste é avaliar seus conhecimentos em organização, estilo, boas práticas e habilidades em front-end. Você será responsável por desenvolver um layout de uma lista usando uma API Rest.

1. A aplicação deve mostrar primeiramente uma listagem de compras com todas as informações conforme o layout;
2. Ao clicar em mais opções representado por "..." deverá exibir os detalhes da compra;
3. O primeiro acesso deve realizado uma requisição no endpoint, os demais "acessos/refresh" devem usar os dados do *_localStorage/IndexedDB_*.

**Layout: Listagem de compras**
![Layout: Listagem de compras](https://bitbucket.org/trex-company/test-front-end/raw/ace5dd08d39dd767e961a610d445b19b1d854ddf/assets/images/layout-order-list.png)

**Layout: Detalhes da compra**
![Layout: Listagem de compras](https://bitbucket.org/trex-company/test-front-end/raw/ace5dd08d39dd767e961a610d445b19b1d854ddf/assets/images/layout-order-details.png)

O tempo ideal para realização da tarefa é de **3 dias**. Também consideramos que, se demorar um pouco mais do que isso, mas entregar um projeto melhor estruturado, também será tão válido quanto o de 3 dias.

- Prazo ideal: 3 dias
- Prazo máximo: 7 dias

### Para visualizar o [layout test-front-end no zeplin](https://zpl.io/brQZ7vL):

1. Acesse [https://zpl.io/brQZ7vL](https://zpl.io/brQZ7vL)
2. Logue-se com o usuário: **Test_FrontEnd**, senha: **b$Y3TK!I1h6y**
3. Navegue entre os designs no projeto "Test FrontEnd", tenha acesso a paleta de cores e aos assets ao lado direito, nas abas "Colors" e "Assets" (imagens abaixo).

![Aba Colors](https://bitbucket.org/trex-company/test-front-end/raw/723b93acc6b0b891044cb3276f0c4c9a83fff1ce/assets/images/tab-colors.png)

![Aba Assets](https://bitbucket.org/trex-company/test-front-end/raw/723b93acc6b0b891044cb3276f0c4c9a83fff1ce/assets/images/tab-assets.png)

### Breakpoints:

| Nome do breakpoint | Largura mínima | Descrição                         |
| ------------------ | -------------- | --------------------------------- |
| phone              | 320px          | Breakpoint para smartphones       |
| tablet             | 768px          | Breakpoint para tablets           |
| desktop            | 1024px         | Breakpoint para desktops comuns   |
| monitor            | 1280px         | Breakpoints para desktops grandes |

### Endpoint inicial para listagem:

> GET https://test-front-end-api.herokuapp.com/api/v1/orders

- Enviar no **headers** "authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyMSIsImlzcyI6ImthZWwucm9uYWxkQG9vbGxvby5vcmciLCJpYXQiOjE1ODUxNTIzMDgsImV4cCI6MTU4NTE1NDEwOH0.HWH0YN7DgUcuHAlx0TCgeqHD9lCGEou5mfZqwVeUQ0k";
- O endpoint tem um delay de **até 10 segundos**, isso é proposital;
- A partir deste ponto utilizar o *_localStorage/IndexedDB_* para persistir localmente as informações.

## O Desafio / O que será avaliado:

1. Readme informando passo a passo como e o que instalar, quais as versões e como executar a aplicação;
2. Estruturação do código / Componentização;
3. Manutenção do código;
4. Maior escalabilidade / Menor interdependência dos componentes;
5. Devem existir duas rotas (routes), uma para a listagem das compras e uma para os detalhes da compra;
6. Componentização do CSS;
7. Compatível com Microsoft Edge (Cross-browsing);
8. Utilização correta do git (commits curtos e contínuos);
9. Testes unitários

### Bonus / O que será considerado bonus:

1. Dockerização da aplicação;
2. Internacionalização (pt-BR e en-US);
3. Utilização de environments;
4. Configuração de script de release;
5. Substituição do *_localStorage/IndexedDB_* por *Redux + Redux Persist*;
6. No pre-commit "rodar" uma verificação da qualidade do código e os testes unitários;
7. Uso de ES6 / ES7;
8. Utilizar alguma metodologia de desenvolvimento: ([BEM](http://getbem.com/introduction/), [OOCSS](http://oocss.org/), [SMACSS](https://smacss.com/) ou [SUITCSS](https://suitcss.github.io/));
9. Feedbacks visuais para o usuário (loadding, alert, notification,...);
10. Permitir edição;

## Submissão / O que você precisará fazer:

Uma das duas opções abaixo é imprescindível que seja feita. Não aceitaremos pacote .zip, .rar ou qualquer outra extensão/arquivo que não seja enviado via GIT.

* Faça um fork desse repositório e clone para a sua máquina;
* Crie sua aplicação;
* Faça os seus commits no projeto;
* Avise-nos via e-mail quando finalizar (junto com o link para o seu repositório público).

OU (ganha pontos a mais):

* Clone esse repositório para sua máquina.
* Crie uma branch com o nome *test/v1/front-end-seunome*.
* Faça os seus commits na sua branch.
* Dê um pull request da sua branch para a *master*.

Caso tenha ficado alguma dúvida entre em contato com [Cesar Filho](mailto:cesarbsfilho@yahoo.com.br).

**Boa Sorte!**